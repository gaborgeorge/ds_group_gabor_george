package DAO;

import DTO.FlightDTO;
import Entities.Flight;

import java.util.List;

public interface FlightDAO {

    int insertFlight(Flight flight);

    Flight getFlight(int flightNo);

    void updateFlight(Flight flight);

    void deleteFlight(int flightNo);

    List<Flight> getAll();
}
