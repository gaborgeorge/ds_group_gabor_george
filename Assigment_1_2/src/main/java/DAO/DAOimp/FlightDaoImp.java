package DAO.DAOimp;

import DAO.FlightDAO;
import Entities.Flight;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.query.Query;

import java.util.List;

@SuppressWarnings("ALL")
public class FlightDaoImp implements FlightDAO {

    private SessionFactory factory;

    public FlightDaoImp(SessionFactory factory) {
        this.factory = factory;
    }

    public List<Flight> getAll() {
        Session session = factory.openSession();
        List<Flight> flights = null;
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            flights = session.createQuery("from Flight").list();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return flights;
    }

    public void deleteFlight(int flightNo) {
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            Query query = session.createQuery("delete Flight where flightNo = :flightNo");
            query.setParameter("flightNo", flightNo);
            query.executeUpdate();
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public void updateFlight(Flight flight) {
        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            session.update(flight);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
    }

    public int insertFlight(Flight flight) {
        int flightId = -1;

        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            flightId = (Integer) session.save(flight);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return flightId;
    }

    public Flight getFlight(int flightNo) {
        Flight flight = null;

        Session session = factory.openSession();
        Transaction tx = null;
        List<Flight> result = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from Flight where flightNo = :flightNo");
            query.setParameter("flightNo", flightNo);
            result = query.list();
            tx.commit();
            if(result != null && result.isEmpty() != true) {
                flight = result.get(0);
            }
        } catch (Exception e) {
            if(tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return flight;
    }
}
