package DAO.DAOimp;

import DAO.UserDAO;
import Entities.User;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

@SuppressWarnings("ALL")
public class UserDaoImp implements UserDAO {
    private SessionFactory factory;

    public UserDaoImp(SessionFactory factory) {
        this.factory = factory;
    }

    public int insertUser(User user) {
        int userId = -1;

        Session session = factory.openSession();
        Transaction tx = null;
        try{
            tx = session.beginTransaction();
            userId = (Integer) session.save(user);
            tx.commit();
        } catch (Exception e) {
            if (tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }

        return userId;
    }

    public User findUserByName(String username) {
        User user = null;

        Session session = factory.openSession();
        Transaction tx = null;
        List<User> result = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from User where username = :username");
            query.setParameter("username", username);
            result = query.list();
            tx.commit();
            if(result != null && result.isEmpty() != true) {
                user = result.get(0);
            }
        } catch (Exception e) {
            if(tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return user;
    }

    // George - 1234 - USER
    // Georgel - 1234 - ADMIN
    // Andrei - 2222 - USER
    // Andreea - 1111 - ADMIN

    public static void main(String[] args) {
        UserDAO userDao = new UserDaoImp(new Configuration().configure().addAnnotatedClass(User.class).buildSessionFactory());
        User user = new User();

        System.out.println(userDao.insertUser(user));
    }
}
