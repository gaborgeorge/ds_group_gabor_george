package DAO.DAOimp;

import DAO.CityDAO;
import Entities.City;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import org.hibernate.cfg.Configuration;
import org.hibernate.query.Query;

import java.util.List;

@SuppressWarnings("ALL")
public class CityDaoImp implements CityDAO {
    private SessionFactory factory;

    public CityDaoImp(SessionFactory factory) {
        this.factory = factory;
    }

    public City findCity(String name) {
        City city = null;

        Session session = factory.openSession();
        Transaction tx = null;
        List<City> result = null;

        try {
            tx = session.beginTransaction();
            Query query = session.createQuery("from City where name = :name");
            query.setParameter("name", name);
            result = query.list();
            tx.commit();
            if(result != null && result.isEmpty() != true) {
                city = result.get(0);
            }
        } catch (Exception e) {
            if(tx != null) {
                tx.rollback();
            }
            e.printStackTrace();
        } finally {
            session.close();
        }
        return city;
    }

    public static void main(String[] args) {
        CityDAO cityDao = new CityDaoImp(new Configuration().configure().addAnnotatedClass(City.class).buildSessionFactory());
        City city = new City();

        System.out.println(cityDao.findCity("Cluj-Napoca"));
    }
}
