package DAO;

import DTO.UserDTO;
import Entities.User;

public interface UserDAO {
    @SuppressWarnings("unchecked")
    int insertUser(User user);

    @SuppressWarnings("unchecked")
    User findUserByName(String username);
}
