package DAO;

import Entities.City;

public interface CityDAO {
    public City findCity(String name);
}
