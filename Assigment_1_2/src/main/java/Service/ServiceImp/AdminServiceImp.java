package Service.ServiceImp;

import DAO.CityDAO;
import DAO.DAOimp.CityDaoImp;
import DAO.DAOimp.FlightDaoImp;
import DAO.FlightDAO;
import DTO.FlightDTO;
import Entities.City;
import Entities.Flight;
import Service.AdminService;
import org.hibernate.cfg.Configuration;

import java.text.DateFormat;
import java.text.Format;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static DTO.FlightDTO.getDTO;

public class AdminServiceImp implements AdminService {

    private CityDAO cityDAO = new CityDaoImp(new Configuration().configure().addAnnotatedClass(City.class).buildSessionFactory());
    private FlightDAO flightDAO = new FlightDaoImp(new Configuration().configure().addAnnotatedClass(Flight.class).addAnnotatedClass(City.class).buildSessionFactory());

    public void deleteFlight(int flightNo) {
        flightDAO.deleteFlight(flightNo);
    }

    public String updateFlight(int flightNo, FlightDTO flightDTO) {
        Flight flight = flightDAO.getFlight(flightNo);
        if(flight == null) {
            return "Wrong flight number";
        }

        Flight updatedFlight = new Flight();

        boolean update = false;

        if(flightDTO.getFlightNo() == -1) {
            updatedFlight.setFlightNo(flight.getFlightNo());
        } else if(flightDTO.getFlightNo() != flight.getFlightNo()){
            updatedFlight.setFlightNo(flightDTO.getFlightNo());
            update = true;
            System.out.println("Flight no");
        }

        if(flightDTO.getAirplaneType() == null) {
            updatedFlight.setAirplaneType(flight.getAirplaneType());
        } else if(flightDTO.getAirplaneType().equals(flight.getAirplaneType()) == false){
            updatedFlight.setAirplaneType(flightDTO.getAirplaneType());
            update = true;
            System.out.println("Airplane type");
        }

        System.out.println(flightDTO.getDepartureCity());
        if(flightDTO.getDepartureCity() == null) {
            updatedFlight.setDepartureCity(flight.getDepartureCity());
        } else {
            City city = cityDAO.findCity(flightDTO.getDepartureCity());
            if (city != null) {
                if(city.getName().equals(flight.getDepartureCity()) == false) {
                    updatedFlight.setDepartureCity(city);
                    update = true;
                    System.out.println("Departure City");
                }
            } else {
                return "Wrong City";
            }
        }

        if(flightDTO.getDepartureDate() == null && flightDTO.getDepartureHour() == null) {
            updatedFlight.setDepartureDateHour(flight.getDepartureDateHour());
        } else {
            Format formatter = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String[] parts = formatter.format(flight.getDepartureDateHour()).split(" ");
            String departureDate = parts[0];
            String departureHour = parts[1];
            String newDepartureDate = departureDate;
            String newDepartureHour = departureHour;

            // verificam ca cel putin una se modifica
            if(flightDTO.getDepartureDate() != null && flightDTO.getDepartureDate().equals(departureDate) == false) {
                newDepartureDate = flightDTO.getDepartureDate();
                update = true;
            }
            if(flightDTO.getDepartureHour() != null && flightDTO.getDepartureHour().equals(departureHour) == false) {
                newDepartureHour = flightDTO.getDepartureHour();
                update = true;
            }
            if(update == true) {
                DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
                Date departureDataHour = null;
                try {
                    departureDataHour = format.parse(newDepartureDate + " " + newDepartureHour);
                    updatedFlight.setDepartureDateHour(departureDataHour);
                    update = true;
                } catch (Exception e) {
                    return "Wrong date or time format. Please respect yyyy-mm-dd hh:mm:ss";
                }
            }
        }

        if(flightDTO.getArrivalCity() == null) {
            updatedFlight.setArrivalCity(flight.getArrivalCity());
        } else {
            City city = cityDAO.findCity(flightDTO.getArrivalCity());
            if (city != null) {
                if(city.getName().equals(flight.getArrivalCity()) == false) {
                    updatedFlight.setArrivalCity(city);
                    update = true;
                    System.out.println("Arrival city");
                }
            } else {
                return "Wrong City";
            }
        }

        if(flightDTO.getArrivalDate() == null && flightDTO.getArrivalHour() == null) {
            updatedFlight.setArrivalDateHour(flight.getArrivalDateHour());
        } else {
            Format formatter = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss");
            String[] parts = formatter.format(flight.getArrivalDateHour()).split(" ");
            String arrivalDate = parts[0];
            String arrivalHour = parts[1];
            System.out.println(arrivalDate);
            System.out.println(arrivalHour);
            String newArrivalDate = arrivalDate;
            String newArrivalHour = arrivalHour;

            // verificam ca cel putin una se modifica
            if(flightDTO.getArrivalDate() != null && flightDTO.getArrivalDate().equals(arrivalDate) == false) {
                newArrivalDate = flightDTO.getArrivalDate();
                update = true;
            }
            if(flightDTO.getArrivalHour() != null && flightDTO.getArrivalHour().equals(arrivalHour) == false) {
                newArrivalHour = flightDTO.getArrivalHour();
                update = true;
            }
            System.out.println(newArrivalDate);
            System.out.println(newArrivalHour);
            if(update == true) {
                DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
                Date arrivalDataHour = null;
                try {
                    arrivalDataHour = format.parse(newArrivalDate + " " + newArrivalHour);
                    updatedFlight.setArrivalDateHour(arrivalDataHour);
                    update = true;
                } catch (Exception e) {
                    return "Wrong date or time format. Please respect yyyy-mm-dd hh:mm:ss";
                }
            }
        }
        System.out.println(updatedFlight);
        if(update == false) {
            return "Flight does not need an update";
        }
        updatedFlight.setId(flight.getId());
        flightDAO.updateFlight(updatedFlight);

        return "Flight updated";
    }

    public FlightDTO findFlight(int flightNo) {
        Flight flight = flightDAO.getFlight(flightNo);
        return getDTO(flight);
    }

    public String createFlight(FlightDTO flightDTO) {
        // get dates and times
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
        Date departureDataHour = null;
        try {
            System.out.println(flightDTO.getDepartureDate() + " " + flightDTO.getDepartureHour());
            departureDataHour = format.parse(flightDTO.getDepartureDate() + " " + flightDTO.getDepartureHour());
        } catch (ParseException e) {
            e.printStackTrace();
            return "Wrong departure date and time format";
        }
        Date arrivalDataHour = null;
        try {
            arrivalDataHour = format.parse(flightDTO.getArrivalDate() + " " + flightDTO.getArrivalHour());
        } catch (ParseException e) {
            e.printStackTrace();
            return "Wrong arrival date and time format";
        }

        // get cities
        City departureCity = cityDAO.findCity(flightDTO.getDepartureCity());
        if(departureCity == null) {
            return "Wrong departure city";
        }
        City arrivalCity = cityDAO.findCity(flightDTO.getArrivalCity());
        if(arrivalCity == null) {
            return "Wrong arrival city";
        }

        Flight flight = new Flight(flightDTO.getFlightNo(), flightDTO.getAirplaneType(), departureCity, departureDataHour, arrivalCity, arrivalDataHour);

        if(-1 == flightDAO.insertFlight(flight)) {
            return "Their was an error save process, a flight with the same number may already exist. Please try again later.";
        }
        return "Create successful";
    }

    public static void main(String[] args) {
        testUtils();
    }

    private static void testUtils() {
        String dateTime = "2018-10-20 12:00:21";
        DateFormat format = new SimpleDateFormat("yyyy-mm-dd hh:mm:ss", Locale.ENGLISH);
        Date date = null;
        try {
            date = format.parse(dateTime);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        System.out.println(date);

        Format formatter = new SimpleDateFormat("yyyy-mm-d hh:mm:ss");
        String s = formatter.format(date);
        System.out.println(s);
    }
}
