package Service.ServiceImp;

import DAO.DAOimp.FlightDaoImp;
import DAO.DAOimp.UserDaoImp;
import DAO.FlightDAO;
import DAO.UserDAO;
import DTO.FlightDTO;
import DTO.LoginDTO;
import DTO.RegisterDTO;
import Entities.City;
import Entities.Flight;
import Entities.User;
import Service.UserService;
import Utils.LocalTimeUtils;
import Utils.PasswordUtils;
import org.hibernate.cfg.Configuration;

import java.util.ArrayList;
import java.util.List;

public class UserServiceImp implements UserService {

    private static final int SALT_LENGTH = 30;

    private UserDAO userDAO = new UserDaoImp(new Configuration().configure().addAnnotatedClass(User.class).buildSessionFactory());
    private FlightDAO flightDAO = new FlightDaoImp(new Configuration().configure().addAnnotatedClass(Flight.class).addAnnotatedClass(City.class).buildSessionFactory());

    public List<String> getFlightLocalTimes(int flightNo) {
        Flight flight = flightDAO.getFlight(flightNo);
        List<String> localTimes = new ArrayList<>();
        if(flight != null) {
            localTimes.add(LocalTimeUtils.getLocalTime(flight.getDepartureCity().getLatitude(), flight.getDepartureCity().getLongitude()));
            localTimes.add(LocalTimeUtils.getLocalTime(flight.getArrivalCity().getLatitude(), flight.getArrivalCity().getLongitude()));
        }
        return localTimes;
    }

    public List<FlightDTO> getFlights() {
        List<Flight> flights = flightDAO.getAll();
        List<FlightDTO> flightDTOS = new ArrayList<>();

        for(Flight flight: flights) {
            flightDTOS.add(FlightDTO.getDTO(flight));
        }

        return flightDTOS;
    }

    public String register(RegisterDTO registerDTO) {
        String salt = PasswordUtils.getSalt(SALT_LENGTH);
        String encriptedPassword = PasswordUtils.generateSecurePassword(registerDTO.getPassword(), salt);

        User user = new User(registerDTO.getUsername(), encriptedPassword, salt, registerDTO.getType());

        if(-1 == userDAO.insertUser(user)) {
            return "Their was an error in the registration process, a user with this username may already exist. Please try again later.";
        }
        return "Registration was successful.";
    }

    public String checkUserValid(LoginDTO loginDTO) {
        //return "ADMIN";
        System.out.println("Checking user credentials");
        User user = userDAO.findUserByName(loginDTO.getUsername());
        if(user == null) {
            return "Wrong username";
        }

        if(true == PasswordUtils.verifyUserPassword(loginDTO.getPassword(), user.getPassword(), user.getSalt())) {
            return user.getType();
        }
        else {
            return "Wrong password";
        }
    }
}
