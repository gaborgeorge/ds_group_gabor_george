package Service;

import DTO.FlightDTO;
import DTO.LoginDTO;
import DTO.RegisterDTO;

import java.util.List;

public interface UserService {

    String register(RegisterDTO registerDTO);

    String checkUserValid(LoginDTO loginDTO);

    List<FlightDTO> getFlights();

    List<String> getFlightLocalTimes(int flightNo);
}
