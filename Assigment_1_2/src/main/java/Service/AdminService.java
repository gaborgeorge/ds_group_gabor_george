package Service;

import DTO.FlightDTO;

public interface AdminService {

    String createFlight(FlightDTO flightDTO);

    FlightDTO findFlight(int flightNo);

    String updateFlight(int flightNo, FlightDTO flightDTO);

    void deleteFlight(int flightNo);
}
