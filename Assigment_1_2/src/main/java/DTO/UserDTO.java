package DTO;

public class UserDTO {

    private Integer id;
    private String username;
    private String password;
    private String salt;
    private String type;

    public UserDTO() {
    }

    public UserDTO(Integer id, String username, String password, String salt, String type) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public class Builder{
        private Integer nestedId;
        private String nestedUsername;
        private String nestedPassword;
        private String nestedSalt;
        private String nestedType;

        public Builder id(int id) {
            this.nestedId = id;
            return this;
        }

        public Builder username(String username) {
            this.nestedUsername = username;
            return this;
        }

        public Builder password(String password) {
            this.nestedPassword = password;
            return this;
        }

        public Builder salt(String salt) {
            this.nestedSalt = salt;
            return this;
        }

        public Builder type(String type) {
            this.nestedType = type;
            return this;
        }

        public UserDTO create() {
            return new UserDTO(nestedId, nestedUsername, nestedPassword, nestedSalt, nestedType);
        }
    }
}
