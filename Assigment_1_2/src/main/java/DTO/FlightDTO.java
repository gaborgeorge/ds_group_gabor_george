package DTO;

import Entities.Flight;

import java.text.Format;
import java.text.SimpleDateFormat;

public class FlightDTO {
    private Integer id;
    private Integer flightNo;
    private String airplaneType;
    private String departureCity;
    private String departureDate;
    private String departureHour;
    private String arrivalCity;
    private String arrivalDate;
    private String arrivalHour;

    public FlightDTO() {
        this.flightNo = -1;
        this.airplaneType = null;
        this.departureCity = null;
        this.departureDate = null;
        this.departureHour = null;
        this.arrivalCity = null;
        this.arrivalDate = null;
        this.arrivalHour = null;
    }

    public FlightDTO(Integer id, Integer flightNo, String airplaneType, String departureCity, String departureDate, String departureHour, String arrivalCity, String arrivalDate, String arrivalHour) {
        this.id = id;
        this.flightNo = flightNo;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDate = departureDate;
        this.departureHour = departureHour;
        this.arrivalCity = arrivalCity;
        this.arrivalDate = arrivalDate;
        this.arrivalHour = arrivalHour;
    }

    public FlightDTO(Integer flightNo, String airplaneType, String departureCity, String departureDate, String departureHour, String arrivalCity, String arrivalDate, String arrivalHour) {
        this.flightNo = flightNo;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDate = departureDate;
        this.departureHour = departureHour;
        this.arrivalCity = arrivalCity;
        this.arrivalDate = arrivalDate;
        this.arrivalHour = arrivalHour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public Integer getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(Integer flightNo) {
        this.flightNo = flightNo;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public String getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(String departureCity) {
        this.departureCity = departureCity;
    }

    public String getDepartureDate() {
        return departureDate;
    }

    public void setDepartureDate(String departureDate) {
        this.departureDate = departureDate;
    }

    public String getDepartureHour() {
        return departureHour;
    }

    public void setDepartureHour(String departureHour) {
        this.departureHour = departureHour;
    }

    public String getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(String arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public String getArrivalDate() {
        return arrivalDate;
    }

    public void setArrivalDate(String arrivalDate) {
        this.arrivalDate = arrivalDate;
    }

    public String getArrivalHour() {
        return arrivalHour;
    }

    public void setArrivalHour(String arrivalHour) {
        this.arrivalHour = arrivalHour;
    }

    @Override
    public String toString() {
        return "FlightDTO{" +
                "id=" + id +
                ", flightNo=" + flightNo +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCity='" + departureCity + '\'' +
                ", departureDate='" + departureDate + '\'' +
                ", departureHour='" + departureHour + '\'' +
                ", arrivalCity='" + arrivalCity + '\'' +
                ", arrivalData='" + arrivalDate + '\'' +
                ", arrivalHour='" + arrivalHour + '\'' +
                '}';
    }

    public static FlightDTO getDTO(Flight flight) {
        FlightDTO flightDTO = null;
        if( flight != null) {
            /* convert flight to dto*/
            Format formatter = new SimpleDateFormat("yyyy-mm-d hh:mm:ss");
            String[] parts = formatter.format(flight.getDepartureDateHour()).split(" ");
            String departureDate = parts[0];
            String departureHour = parts[1];
            parts = formatter.format(flight.getArrivalDateHour()).split(" ");
            String arrivalDate = parts[0];
            String arrivalHour = parts[1];
            /*CHECK split*/
            flightDTO = new FlightDTO(flight.getId(),
                    flight.getFlightNo(),
                    flight.getAirplaneType(),
                    flight.getDepartureCity().getName(),
                    departureDate,
                    departureHour,
                    flight.getArrivalCity().getName(),
                    arrivalDate,
                    arrivalHour);
        }

        return flightDTO;
    }
}
