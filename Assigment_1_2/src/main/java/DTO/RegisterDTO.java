package DTO;

public class RegisterDTO {
    private String username;
    private String password;
    private String passwordConfirm;
    private String type;

    public RegisterDTO() {
    }

    public RegisterDTO(String username, String password, String passwordConfirm, String type) {
        this.username = username;
        this.password = password;
        this.passwordConfirm = passwordConfirm;
        this.type = type;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getPasswordConfirm() {
        return passwordConfirm;
    }

    public void setPasswordConfirm(String passwordConfirm) {
        this.passwordConfirm = passwordConfirm;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public static class Builder {
        private String nestedUsername;
        private String nestedPassword;
        private String nestedPasswordConfirm;
        private String nestedType;

        public Builder username(String username) {
            this.nestedUsername = username;
            return this;
        }

        public Builder password(String password) {
            this.nestedPassword = password;
            return this;
        }

        public Builder passwordConfirm(String passwordConfirm) {
            this.nestedPasswordConfirm = passwordConfirm;
            return this;
        }

        public Builder type(String type) {
            this.nestedType = type;
            return this;
        }

        public RegisterDTO create() {
            return new RegisterDTO(nestedUsername, nestedPassword, nestedPasswordConfirm, nestedType);
        }
    }
}
