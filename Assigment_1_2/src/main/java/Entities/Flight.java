package Entities;

import javax.persistence.*;
import java.util.Date;

@Entity
@Table(name = "flight")
public class Flight {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "flightNo", nullable = false, unique = true)
    private int flightNo;

    @Column(name = "airplaneType", nullable = false, length = 45)
    private String airplaneType;

    @ManyToOne
    @JoinColumn(name = "departureCity", referencedColumnName = "id")
    private City departureCity;

    @Column(name = "departureDateHour", nullable = false)
    private Date departureDateHour;

    @ManyToOne
    @JoinColumn(name = "arrivalCity", referencedColumnName = "id")
    private City arrivalCity;

    @Column(name = "arrivalDateHour", nullable = false)
    private Date arrivalDateHour;

    public Flight() {
    }

    public Flight(Integer flightNo, String airplaneType, City departureCity, Date departureDateHour, City arrivalCity, Date arrivalDateHour) {
        this.flightNo = flightNo;
        this.airplaneType = airplaneType;
        this.departureCity = departureCity;
        this.departureDateHour = departureDateHour;
        this.arrivalCity = arrivalCity;
        this.arrivalDateHour = arrivalDateHour;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public int getFlightNo() {
        return flightNo;
    }

    public void setFlightNo(int flightNo) {
        this.flightNo = flightNo;
    }

    public String getAirplaneType() {
        return airplaneType;
    }

    public void setAirplaneType(String airplaneType) {
        this.airplaneType = airplaneType;
    }

    public City getDepartureCity() {
        return departureCity;
    }

    public void setDepartureCity(City departureCity) {
        this.departureCity = departureCity;
    }

    public Date getDepartureDateHour() {
        return departureDateHour;
    }

    public void setDepartureDateHour(Date departureDateHour) {
        this.departureDateHour = departureDateHour;
    }

    public City getArrivalCity() {
        return arrivalCity;
    }

    public void setArrivalCity(City arrivalCity) {
        this.arrivalCity = arrivalCity;
    }

    public Date getArrivalDateHour() {
        return arrivalDateHour;
    }

    public void setArrivalDateHour(Date arrivalDateHour) {
        this.arrivalDateHour = arrivalDateHour;
    }

    @Override
    public String toString() {
        return "Flight{" +
                "id=" + id +
                ", flightNo=" + flightNo +
                ", airplaneType='" + airplaneType + '\'' +
                ", departureCity=" + departureCity +
                ", departureDateHour=" + departureDateHour +
                ", arrivalCity=" + arrivalCity +
                ", arrivalDateHour=" + arrivalDateHour +
                '}';
    }
}
