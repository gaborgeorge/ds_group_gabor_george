package Entities;

import javax.persistence.*;

@Entity
@Table(name = "User")
public class User implements java.io.Serializable {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id", unique = true, nullable = false)
    private Integer id;

    @Column(name = "username", unique = true, nullable = false, length = 45)
    private String username;

    @Column(name = "password", nullable = false, length = 80)
    private String password;

    @Column(name = "salt", nullable = false, length = 45)
    private String salt;

    @Column(name = "type", nullable = false, length = 45)
    private String type;

    public User() {
        this.username = "Default";
        this.password = "Default";
        this.salt = "Default";
        this.type = "USER";
    }

    public User(String username, String password, String salt, String type) {
        this.id = -1;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.type = type;
    }

    public User(Integer id, String username, String password, String salt, String type) {
        this.id = id;
        this.username = username;
        this.password = password;
        this.salt = salt;
        this.type = type;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSalt() {
        return salt;
    }

    public void setSalt(String salt) {
        this.salt = salt;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "User{" +
                "id=" + id +
                ", username='" + username + '\'' +
                ", password='" + password + '\'' +
                ", salt='" + salt + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}
