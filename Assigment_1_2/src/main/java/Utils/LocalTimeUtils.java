package Utils;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.StringReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.sql.Time;
import java.time.LocalTime;
import java.util.List;
import javax.print.DocFlavor;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;

public class LocalTimeUtils {
    private static final String LOCAL_TIME_SERVICE = "http://www.new.earthtools.org/timezone/";

    private static StringBuffer getResponse(String url)
    {
        try {
            URL obj = new URL(url);
            HttpURLConnection con = (HttpURLConnection) obj.openConnection();
            int responseCode = con.getResponseCode();
            System.out.println("Response Code : " + responseCode);

            BufferedReader in = new BufferedReader(
                    new InputStreamReader(con.getInputStream()));
            String inputLine;
            StringBuffer response = new StringBuffer();
            while ((inputLine = in.readLine()) != null) {
                response.append(inputLine);
            }
            in.close();

            return response;
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        return null;
    }

    private static String parseResponse(StringBuffer response){
        // System.out.println(response.toString());
        Document doc = null;
        String localTime = null;
        try {
            doc = DocumentBuilderFactory.newInstance().newDocumentBuilder()
                    .parse(new InputSource(new StringReader(response.toString())));
            NodeList errNodes = doc.getElementsByTagName("timezone");
            if (errNodes.getLength() > 0) {
                Element err = (Element)errNodes.item(0);
                localTime = err.getElementsByTagName("localtime").item(0).getTextContent();
            }
        } catch (SAXException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        } catch (ParserConfigurationException e) {
            e.printStackTrace();
        }

        if(localTime != null) {
            String[] parts = localTime.split(" ");
            localTime = parts[3];
        }
        return localTime;
    }

    public static String getLocalTime(double langitude, double longitude)
    {
        StringBuffer response = getResponse(LOCAL_TIME_SERVICE + langitude + '/' + longitude);
        if(response == null){
            return null;
        }
        System.out.println(response);
        return parseResponse(response);
    }

    public static void main(String[] args) {
        String time = getLocalTime(40.71417, -74.00639);

        System.out.println(time);
    }
}