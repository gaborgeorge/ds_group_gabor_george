package Servlet.Func;

import DTO.RegisterDTO;
import Service.ServiceImp.UserServiceImp;
import Service.UserService;
import Servlet.ResponseBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Register")
public class RegisterServlet extends HttpServlet {

    private UserService userService = new UserServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) {
        String username = request.getParameter("username");
        String password = request.getParameter("password");
        String passwordConfirm = request.getParameter("password_confirm");
        String type = request.getParameter("type");
        /*NOTE user may already be registered*/
        RegisterDTO registerDTO = new RegisterDTO.Builder()
                                        .username(username)
                                        .password(password)
                                        .passwordConfirm(passwordConfirm)
                                        .type(type)
                                        .create();
        String result = userService.register(registerDTO);
        ResponseBuilder.message(response, result);
        System.out.println(result);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("register.html");
    }
}
