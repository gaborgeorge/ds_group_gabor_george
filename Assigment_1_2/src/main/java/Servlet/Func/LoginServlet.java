package Servlet.Func;

import DTO.LoginDTO;
import Service.ServiceImp.UserServiceImp;
import Service.UserService;
import Servlet.ResponseBuilder;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/Login")
public class LoginServlet extends HttpServlet {

    private UserService userService = new UserServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String username = request.getParameter("username");
        String password = request.getParameter("password");

        LoginDTO loginDTO = new LoginDTO(username, password);
        String message = userService.checkUserValid(loginDTO);
        if(message.equals("USER") == true){
            request.getSession().setAttribute("name", "user");
            response.sendRedirect("client.html");
        }
        else if(message.equals("ADMIN") == true) {
            request.getSession().setAttribute("name", "admin");
            response.sendRedirect("admin.html");
        }
        else{
            ResponseBuilder.message(response, message);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        RequestDispatcher rd=request.getRequestDispatcher("login.html");
        rd.forward(request,response);
    }
}
