package Servlet.Client;

import DTO.FlightDTO;
import Service.ServiceImp.UserServiceImp;
import Service.UserService;
import Servlet.ResponseBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.List;

@SuppressWarnings("ALL")
@WebServlet("/Client")
public class ClientServlet extends HttpServlet {

    private UserService userService = new UserServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flightNoS = request.getParameter("FlightNo");

        String message = null;
        int flightNo = 0;
        if(flightNoS == null || flightNoS.equals("") == true) {
            message = "Flight number must have a value";
        }
        else {
            boolean ok = true;
            try {
                flightNo = Integer.parseInt(flightNoS);
                if( flightNo <= 0) {
                    ok = false;
                }
            } catch (Exception e) {
                ok = false;
                e.printStackTrace();
            }
            if(ok == false) {
                message = "Flight number must be a positive nonzero integer";
            }
        }
        if(message == null) {
            List<String> localTimes = userService.getFlightLocalTimes(flightNo);
            if(localTimes.isEmpty() == false) {
                System.out.println(localTimes.get(0));
                System.out.println(localTimes.get(1));
                message = "Departure City: " + localTimes.get(0) + "<br>" + "Arrival City: " + localTimes.get(1);
            } else {
                message = "Wrong Flight number";
            }
        }
        ResponseBuilder.message(response, message);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        List<FlightDTO> flights = userService.getFlights();

        ResponseBuilder.messageFlights(response, flights);

        for(FlightDTO flight: flights) {
            System.out.println(flight);
        }
    }
}
