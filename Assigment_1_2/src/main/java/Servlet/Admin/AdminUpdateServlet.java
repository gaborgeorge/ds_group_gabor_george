package Servlet.Admin;

import DTO.FlightDTO;
import Entities.Flight;
import Service.AdminService;
import Service.ServiceImp.AdminServiceImp;
import Servlet.ResponseBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("ALL")
@WebServlet("/AdminUpdate")
public class AdminUpdateServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flightNoS = request.getParameter("oldFlightNoUpdate");
        String newFlightNoS = request.getParameter("flightNoUpdate");
        String newFlightType = request.getParameter("airplaneTypeUpdate");
        String newDepartureCity = request.getParameter("departureCityUpdate");
        String newDepartureTime =  request.getParameter("departureTimeUpdate");
        String newDepartureDate =  request.getParameter("departureDateUpdate");
        String newArrivalCity = request.getParameter("arrivalCityUpdate");
        String newArrivalTime = request.getParameter("arrivalTimeUpdate");
        String newArrivalDate = request.getParameter("arrivalDateUpdate");

        boolean ok = true;
        FlightDTO flightDTO = new FlightDTO();
        String message = null;

        Integer flightNo = 0;
        if(flightNoS != null && flightNoS.equals("") == false) {
            try {
                flightNo = Integer.parseInt(flightNoS);
                if(flightNo <= 0)
                {
                    message = "Flight number must be >= 0";
                }
            } catch (Exception e) {
                e.printStackTrace();
                message = "Flight number must be >= 0";
            }
        } else {
            message = "Flight number must have a value";
        }

        if(newFlightNoS != null && newFlightNoS.equals("") == false) {
            try {
                int newFlightNo = Integer.parseInt(newFlightNoS);
                if(newFlightNo <= 0)
                {
                    message = "The new Flight number must be >= 0";
                } else {
                    flightDTO.setFlightNo(newFlightNo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                message = "The new Flight number must be >= 0";
            }
        }

        if(newFlightType != null && newFlightType.equals("") == false) {
            flightDTO.setAirplaneType(newFlightType);
        }

        if(newDepartureCity != null && newDepartureCity.equals("") == false) {
            flightDTO.setDepartureCity(newDepartureCity);
        }

        if(newDepartureDate != null && newDepartureDate.equals("") == false) {
            flightDTO.setDepartureDate(newDepartureDate);
        }

        if(newDepartureTime != null && newDepartureTime.equals("") == false) {
            flightDTO.setDepartureHour(newDepartureTime);
        }

        if(newArrivalCity != null && newArrivalCity.equals("") == false) {
            flightDTO.setArrivalCity(newArrivalCity);
        }

        if(newArrivalDate != null && newArrivalDate.equals("") == false) {
            flightDTO.setArrivalDate(newArrivalDate);
        }

        if(newArrivalTime != null && newArrivalTime.equals("") == false) {
            flightDTO.setArrivalHour(newArrivalTime);
        }

        if(message == null) {
                message = adminService.updateFlight(flightNo, flightDTO);
        }
        System.out.println(message);
        ResponseBuilder.message(response, message);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("admin.html");
    }
}
