package Servlet.Admin;

import Service.AdminService;
import Service.ServiceImp.AdminServiceImp;
import Servlet.ResponseBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@SuppressWarnings("ALL")
@WebServlet("/AdminDelete")
public class AdminDeleteServlet extends HttpServlet {

    AdminService adminService = new AdminServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flightNoS = request.getParameter("flightNoDelete");

        String message = null;
        Integer flightNo = 0;
        if(flightNoS != null && flightNoS.equals("") == false) {
            try {
                flightNo = Integer.parseInt(flightNoS);
                if(flightNo <= 0)
                {
                    message = "Flight number must be >= 0";
                }
            } catch (Exception e) {
                e.printStackTrace();
                message = "Flight number must be >= 0";
            }
        } else {
            message = "Flight number must have a value";
        }

        if(message == null) {
            adminService.deleteFlight(flightNo);
            message = "Delete Success";
        }
        ResponseBuilder.message(response, message);
        System.out.println(message);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("admin.html");
    }
}
