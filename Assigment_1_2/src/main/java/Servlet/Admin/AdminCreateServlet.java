package Servlet.Admin;

import DTO.FlightDTO;
import Service.AdminService;
import Service.ServiceImp.AdminServiceImp;
import Servlet.ResponseBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/AdminInsert")
public class AdminCreateServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String flightNoS = request.getParameter("flightNoPost");
        String airplaneType = request.getParameter("airplaneTypePost");
        String departureCity = request.getParameter("departureCityPost");
        String departureTime = request.getParameter("departureTimePost");
        String departureDate = request.getParameter("departureDatePost");
        String arrivalCity = request.getParameter("arrivalCityPost");
        String arrivalTime = request.getParameter("arrivalTimePost");
        String arrivalDate = request.getParameter("arrivalDatePost");

        /* YYYY-MM-DD HH:MM:SS */
        /* o functie care sa imi converteasca la int */
        FlightDTO flightDTO = new FlightDTO();
        String message = null;
        boolean ok = true;
        if(flightNoS != null && flightNoS.equals("") == false) {
            try {
                int flightNo = Integer.parseInt(flightNoS);
                if(flightNo <= 0)
                {
                    message = "The new Flight number must be >= 0";
                    ok = false;
                } else {
                    flightDTO.setFlightNo(flightNo);
                }
            } catch (Exception e) {
                e.printStackTrace();
                message = "The new Flight number must be >= 0";
                ok = false;
            }
        } else {
            ok = false;
            message = "Flight number must have a value";
        }

        if(ok == true){
            if(airplaneType != null && airplaneType.equals("") == false) {
                flightDTO.setAirplaneType(airplaneType);
            } else {
                ok = false;
                message = "Flight type must have a value";
            }
        }

        if(ok == true) {
            if (departureCity != null && departureCity.equals("") == false) {
                flightDTO.setDepartureCity(departureCity);
            } else {
                ok = false;
                message = "Arrival City must have a value";
            }
        }

        if(ok == true) {
            if(departureTime != null && departureTime.equals("") == false) {
                flightDTO.setDepartureHour(departureTime);
            } else {
                ok = false;
                message = "Arrival Time must have a value";
            }
        }

        if(ok == true) {
            if(departureDate != null && departureDate.equals("") == false) {
                flightDTO.setDepartureDate(departureDate);
            } else {
                ok = false;
                message = "Arrival Date must have a value";
            }
        }

        if(ok == true) {
            if (arrivalCity != null && arrivalCity.equals("") == false) {
                flightDTO.setArrivalCity(arrivalCity);
            } else {
                ok = false;
                message = "Arrival City must have a value";
            }
        }

        if(ok == true) {
            if( arrivalTime != null && arrivalTime.equals("") == false) {
                flightDTO.setArrivalHour(arrivalTime);
            } else {
                ok = false;
                message = "Arrival Time must have a value";
            }
        }

        if(ok == true) {
            if(arrivalDate != null && arrivalDate.equals("") == false) {
                flightDTO.setArrivalDate(arrivalDate);
            } else {
                message = "Arrival Date must have a value";
            }
        }

        if(message == null) {
            message = adminService.createFlight(flightDTO);
        }
        /*NOTE: show message*/
        System.out.println(message);
        ResponseBuilder.message(response, message);
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("admin.html");
    }
}
