package Servlet.Admin;

import DTO.FlightDTO;
import Service.AdminService;
import Service.ServiceImp.AdminServiceImp;
import Servlet.ResponseBuilder;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@SuppressWarnings("ALL")
@WebServlet("/AdminFind")
public class AdminFindServlet extends HttpServlet {

    private AdminService adminService = new AdminServiceImp();

    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("Executing Find Flight Operation");

        String flightNo = request.getParameter("flightNoGet");

        String message = null;
        int n = 0;
        if(flightNo == null || flightNo.equals("") == true) {
            message = "Flight number must have a value";
        }
        else {
            boolean ok = true;
            try {
                n = Integer.parseInt(flightNo);
                if( n <= 0) {
                    ok = false;
                }
            } catch (Exception e) {
                ok = false;
                e.printStackTrace();
            }
            if(ok == false) {
                message = "Flight number must be a positive nonzero integer";
            }
        }

        if(message == null) {
            FlightDTO flightDTO = adminService.findFlight(n);
            if(flightDTO == null)
            {
                System.out.println("Wrong flight number.");
                ResponseBuilder.message(response, "Wrong flight number.");
            } else {
                System.out.println(flightDTO);
                ArrayList<FlightDTO> flightDTOS = new ArrayList<>();
                flightDTOS.add(flightDTO);
                ResponseBuilder.messageFlights(response, flightDTOS);
            }
        } else {
            ResponseBuilder.message(response, message);
            System.out.println(message);
        }
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        response.sendRedirect("admin.html");
    }
}
