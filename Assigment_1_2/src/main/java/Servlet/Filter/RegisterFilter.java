package Servlet.Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import Servlet.ResponseBuilder;

@WebFilter("/Register")
public class RegisterFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;

        if(request.getMethod().equalsIgnoreCase("POST") == true) {
            String username = request.getParameter("username");
            String password = request.getParameter("password");
            String password_confirm = request.getParameter("password_confirm");
            String type = request.getParameter("type");

            String message = null;
            if (username == null || username.equals("") == true) {
                message = "Username field must have a value";
            } else if (password == null || password.equals("") == true) {
                message = "Password field must have a value";
            } else if (password_confirm == null || password_confirm.equals("") == true) {
                message = "You must confirm your password";
            } else if (password.equals(password_confirm) == false) {
                message = "The password do not match. PLEASE ENTER THE SAME PASSWORD";
            } else if (type.equals("USER") == false && type.equals("ADMIN") == false) {
                message = "Wrong user type. Please choose between user and admin";
            }

            if (message != null) {
                System.out.println(message);
                ResponseBuilder.message(response, message);
                return;
            }
        }
        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
