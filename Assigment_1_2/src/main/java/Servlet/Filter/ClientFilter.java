package Servlet.Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebFilter("/client.html")
public class ClientFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String name = (String) request.getSession().getAttribute("name");

        /* NOTE: add some kind of message to tell the user to log in or that it is not this type of user*/
        /*Check if no one is logged in*/
        if(name == null){
            response.sendRedirect("login.html");
        }
        /*Check if you is logged in is a Client*/
        else if(name.equals("user") == false){
            response.sendRedirect("login.html");
        }
        /* Disable browser caching for this page requested */
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0
        response.setHeader("Expires","0"); //Proxies

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }

}
