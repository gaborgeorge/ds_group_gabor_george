package Servlet.Filter;

import javax.servlet.*;
import javax.servlet.annotation.WebFilter;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebFilter("/admin.html")
public class AdminFilter implements Filter {
    public void destroy() {
    }

    public void doFilter(ServletRequest req, ServletResponse resp, FilterChain chain) throws ServletException, IOException {
        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) resp;
        String name = (String) request.getSession().getAttribute("name");

        /*Note: add message to tell the user why he can't access the page*/
        /*Check if no one is logged in*/
        if(name == null){
            response.sendRedirect("login.html");
        }
        /* Check if the one logged in is an Admin */
        else if(name.equals("admin") == false){
            response.sendRedirect("login.html");
        }
        /* Disable browser caching for the page requested */
        response.setHeader("Cache-Control", "no-cache, no-store, must-revalidate"); // HTTP 1.1
        response.setHeader("Pragma", "no-cache"); // HTTP 1.0
        response.setHeader("Expires","0"); //Proxies

        chain.doFilter(req, resp);
    }

    public void init(FilterConfig config) throws ServletException {

    }
}
