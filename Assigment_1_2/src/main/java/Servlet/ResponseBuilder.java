package Servlet;

import DTO.FlightDTO;

import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

public class ResponseBuilder {

    private static final String htmlHeader = "<html><head><meta charset=\"UTF-8\"><title></title> <link href=\"style.css\" rel=\"stylesheet\"></head>";

    public static void message(HttpServletResponse response, String message) {
        PrintWriter printWriter = null;
        try {
            printWriter = response.getWriter();
            printWriter.println(htmlHeader);
            printWriter.println("<table width = 300 align = \"center\">");
            printWriter.println("<tr align = \"center\"><td>");
            printWriter.println(message);
            printWriter.println("</td></tr></table>");
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static void messageFlights(HttpServletResponse response, List<FlightDTO> flightDTOS) {
        PrintWriter printWriter = null;
        try {
            printWriter = response.getWriter();
            printWriter.println(htmlHeader);
            printWriter.println("<table widht = 1000 align = \"center\">");
            printWriter.println("<tr align = \"center\">");
            printWriter.println("<th>ID</th>");
            printWriter.println("<th>FLIGHT NUMBER</th>");
            printWriter.println("<th>AIRPLANE TYPE</th>");
            printWriter.println("<th>DEPARTURE CITY</th>");
            printWriter.println("<th>DEPARTURE DATE</th>");
            printWriter.println("<th>DEPARTURE TIME</th>");
            printWriter.println("<th>ARRIVAL CITY</th>");
            printWriter.println("<th>ARRIVAL DATE</th>");
            printWriter.println("<th>ARRIVAL TIME</th>");
            printWriter.println("</tr>");
            for(FlightDTO flightDTO: flightDTOS) {
                printWriter.println("<tr align = \"center\">");
                printWriter.println("<td>" + flightDTO.getId() + "</td>");
                printWriter.println("<td>" + flightDTO.getFlightNo() + "</td>");
                printWriter.println("<td>" + flightDTO.getAirplaneType() + "</td>");
                printWriter.println("<td>" + flightDTO.getDepartureCity() + "</td>");
                printWriter.println("<td>" + flightDTO.getDepartureDate() + "</td>");
                printWriter.println("<td>" + flightDTO.getDepartureHour() + "</td>");
                printWriter.println("<td>" + flightDTO.getArrivalCity() + "</td>");
                printWriter.println("<td>" + flightDTO.getArrivalDate() + "</td>");
                printWriter.println("<td>" + flightDTO.getArrivalHour() + "</td>");
                printWriter.println("</tr>");
            }
            printWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
