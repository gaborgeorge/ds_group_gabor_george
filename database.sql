-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: localhost    Database: airport
-- ------------------------------------------------------
-- Server version	5.7.20-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `city`
--

DROP TABLE IF EXISTS `city`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `city` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(45) NOT NULL,
  `latitude` double NOT NULL,
  `longitude` double NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name_UNIQUE` (`name`)
) ENGINE=InnoDB AUTO_INCREMENT=8 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `city`
--

LOCK TABLES `city` WRITE;
/*!40000 ALTER TABLE `city` DISABLE KEYS */;
INSERT INTO `city` VALUES (2,'Cluj-Napoca',46.770439,23.591423),(3,'Timisoara',45.75372,21.22571),(4,'Sibiu',45.7983273,24.1255826),(5,'Oradea',47.0667,21.9333),(6,'Liverpool',53.4083714,-2.9915726),(7,'Dubai',25.276987,55.296249);
/*!40000 ALTER TABLE `city` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `flight`
--

DROP TABLE IF EXISTS `flight`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `flight` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `flightNo` int(11) NOT NULL,
  `airplaneType` varchar(45) NOT NULL,
  `departureCity` int(11) NOT NULL,
  `departureDateHour` datetime NOT NULL,
  `arrivalCity` int(11) NOT NULL,
  `arrivalDateHour` datetime NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `flightNo_UNIQUE` (`flightNo`),
  KEY `departureCity_idx` (`departureCity`),
  KEY `arrivalCity_idx` (`arrivalCity`),
  CONSTRAINT `arrivalCity` FOREIGN KEY (`arrivalCity`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `departureCity` FOREIGN KEY (`departureCity`) REFERENCES `city` (`id`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `flight`
--

LOCK TABLES `flight` WRITE;
/*!40000 ALTER TABLE `flight` DISABLE KEYS */;
INSERT INTO `flight` VALUES (1,2,'Economy',4,'2018-01-20 00:30:00',2,'2018-01-20 13:40:00'),(2,4,'Airbust',2,'2018-01-20 00:30:00',4,'2018-01-20 13:40:00');
/*!40000 ALTER TABLE `flight` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `username` varchar(45) NOT NULL,
  `password` varchar(80) NOT NULL,
  `salt` varchar(45) NOT NULL,
  `type` varchar(45) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `username_UNIQUE` (`username`)
) ENGINE=InnoDB AUTO_INCREMENT=21 DEFAULT CHARSET=latin1;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
INSERT INTO `user` VALUES (12,'Andrei','ClerYWxqNIRQos6UIcTof6ILYfjJnFQD03Cm9p7DzB0=','zF8v7CIFQaMdx6ZtNNbLbUoW9KQ9gC','USER'),(13,'Andreea','LH9kTQmLYtXxzQzVrH/IO5dqwan0YhmQVxh9qUxJegk=','QREe2d7F5lO3VG9B78KZmfL3j6pF3d','ADMIN'),(14,'Test','smb9GEKTRnlE9jfpWrD1OeJUxAJ6p1Xx2WU1S5wm2Jc=','XlydIKqHwduBZMpTXovFD0pEPbRy9t','USER'),(15,'George','jqExHs6ZXmqUybQeEye+yHWnDDvW2wxSsVqSyBBhyN8=','kIue3yWB5IaBNy9ynZ4QroV05bcM8u','USER'),(17,'Georgel','MiGrNGYnSxLLZSPzUPO5gWMcoC5GM5Ml4dNS/slVKd8=','iplIO12Uf37kq6BYHzjE7WrG0nB27t','ADMIN'),(18,'George2','kcXDVO5iwZQrz34ansiyb6BAYXPIoaI8h6rnKt/U+8E=','bjmWhNwJvzbZfMVaAy7Rzl3vKNKehm','USER'),(19,'Ana','0gwOEWPQRS4RIQX7NWhwxkuw+x2rwQ4KpVqF5AkIk+E=','UmaG9wmmLh1UvB0vjQdL69LNxbcdDZ','USER'),(20,'Gicu','+V/dAlrc2GL95ID7+ZWDVGRVPvANWvIT12IthqK+jr8=','Jz1Gfn073OGnT0bxcLhrL4OhY7BZg0','USER');
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-11-01 21:25:58
